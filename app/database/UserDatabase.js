const mongoose = require('mongoose')

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost:27017/RakutenWebApp', {useNewUrlParser: true})
    const connection = mongoose.connection
    if (process.env.NODE_ENV === 'test') {
        const Mockgoose = require('mockgoose').Mockgoose
        const mockgoose = new Mockgoose(mongoose);

        mockgoose.prepareStorage()
            .then(() => {
                connection.then((db) => {
                    console.log('Connection to mongo mockgoose was a success')
                    return db
                }).catch((err) => {
                    console.log('Connection to mongo mockgoose was not a success')
                })
            })

    }
    else {
        connection.then((db) => {
            console.log('Connection to mongo was a success')
            return db
        }).catch((err) => {
            console.log('Connection to mongo was not a success')
        })
    }


