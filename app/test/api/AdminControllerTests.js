process.env.NODE_ENV = 'test';
const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');
const req = request(app);
const chai = require('chai')
const chaiHttp = require('chai-http');
chai.use(chaiHttp);


describe('AdminController', () => {
    it('creating a new Admin user', (done) => {
        const data = {
            firstName: 'Admin',
            lastName: 'Adam',
            username: 'adam123',
            email: 'adam123@gmail.com',
            password: 'adam123pass'
        }
        chai.request(app)
            .post('/v1/adminuser/create')
            .send(data)
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                expect(body).to.contain.property('username');
                expect(body).to.contain.property('email');
                expect(body).to.contain.property('password');
                done();
            }).catch(done);
    })

    it('creating a new adminuser with same username', (done) => {
        const data = {
            firstName: 'Admin',
            lastName: 'Adam',
            username: 'admin123',
            email: 'adam123@gmail.com',
            password: 'adam123pass'
        }
        chai.request(app)
            .post('/v1/adminuser/create')
            .send(data)
            .then((res) => {
                expect(res).to.have.status(400);
                const body = res.body;
                expect(res.text).to.contain('Sorry this user is already signed up!');
                expect(body).to.not.contain.property('username');
                expect(body).to.not.contain.property('email');
                expect(body).to.not.contain.property('password');
                done();
            }).catch(done);
    })

    it('creating a new adminuser with blank data', (done) => {
        const data = {
            firstName: 'Admin',
            lastName: 'Adam',
            username: '',
            email: 'adam123@gmail.com',
            password: 'adam123pass'
        }
        chai.request(app)
            .post('/v1/adminuser/create')
            .send(data)
            .then((res) => {
                expect(res).to.have.status(400);
                const body = res.body;
                expect(res.text).to.contain('Please Fill in all fields');
                expect(body).to.not.contain.property('username');
                expect(body).to.not.contain.property('email');
                expect(body).to.not.contain.property('password');
                done();
            }).catch(done);
    })

    it('getting all users ', (done) => {
        chai.request(app)
            .get('/v1/adminuser')
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                expect(body).to.be.a('Array')
                expect(body[0]).to.contain.property('username');
                expect(body[0]).to.contain.property('email');
                expect(body[0]).to.contain.property('password');
                done();
            }).catch(done);
    })

    it('signing in admin user', (done) => {
        const data = {
            username: 'adam123',
            password: 'adam123pass'
        }
        chai.request(app)
            .post('/v1/adminuser/signIn')
            .send(data)
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                expect(body[0]).to.contain.property('username').eq('adam123');
                expect(body[0]).to.contain.property('email');
                expect(body[0]).to.contain.property('password');
                done();
            }).catch(done);
    })

    it('deleting one admin user ', (done) => {
        const username = 'adam123'
        chai.request(app)
            .delete('/v1/adminuser/' + username)
            .then((res) => {
                expect(res).to.have.status(200);
                done();
            }).catch(done);
    })
})