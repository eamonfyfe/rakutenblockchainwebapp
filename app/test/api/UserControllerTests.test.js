process.env.NODE_ENV = 'test';
const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');
const req = request(app);
const chai = require('chai')
const chaiHttp = require('chai-http');
chai.use(chaiHttp);


describe('UserController', () => {
    it('creating a new user', (done) => {
        const data = {
            firstName: 'Jonny',
            lastName: 'doyle',
            username: 'jon12334456456',
            email: 'jon123@gmail.com',
            password: 'jon123pass'
        }
        chai.request(app)
            .post('/v1/user/create')
            .send(data)
            .then((res) => {
                expect(res).to.have.status(400);
                const body = res.body;
                expect(body).to.contain.property('firstName');
                expect(body).to.contain.property('lastName');
                expect(body).to.contain.property('username');
                expect(body).to.contain.property('email');
                expect(body).to.contain.property('password');
                done();
            }).catch(done);
    })

    it('creating a new user with same username', (done) => {
        const data = {
            firstName: 'Jon',
            lastName: 'davy',
            username: 'jon12334456456',
            email: 'jon123@gmail.com',
            password: 'jon123pass'
        }
        chai.request(app)
            .post('/v1/user/create')
            .send(data)
            .then((res) => {
                expect(res).to.have.status(400);
                const body = res.body;
                expect(res.text).to.contain('Sorry this user is already signed up!');
                expect(body).to.not.contain.property('firstName');
                expect(body).to.not.contain.property('lastName');
                expect(body).to.not.contain.property('username');
                expect(body).to.not.contain.property('email');
                expect(body).to.not.contain.property('password');
                done();
            }).catch(done);
    })

    it('creating a new user with blank data', (done) => {
        const data = {
            firstName: 'Jonny',
            lastName: 'doyle',
            username: '',
            email: 'jon123@gmail.com',
            password: 'jon123pass'
        }
        chai.request(app)
            .post('/v1/user/create')
            .send(data)
            .then((res) => {
                expect(res).to.have.status(400);
                const body = res.body;
                expect(res.text).to.contain('Please Fill in all fields');
                expect(body).to.not.contain.property('firstName');
                expect(body).to.not.contain.property('lastName');
                expect(body).to.not.contain.property('username');
                expect(body).to.not.contain.property('email');
                expect(body).to.not.contain.property('password');
                done();
            }).catch(done);
    })

    it('getting all users ', (done) => {
        chai.request(app)
            .get('/v1/user')
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                expect(body).to.be.a('Array')
                expect(body[0]).to.contain.property('firstName');
                expect(body[0]).to.contain.property('lastName');
                expect(body[0]).to.contain.property('username');
                expect(body[0]).to.contain.property('email');
                expect(body[0]).to.contain.property('password');
                done();
            }).catch(done);
    })

    it('getting one user ', (done) => {
        const username = 'jon12334456456'
        chai.request(app)
            .get('/v1/user/' + username)
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                // expect(body[0]).to.contain.value('jon12334456456')
                expect(body[0]).to.contain.property('firstName');
                expect(body[0]).to.contain.property('lastName');
                expect(body[0]).to.contain.property('username').eq('jon12334456456');
                expect(body[0]).to.contain.property('email');
                expect(body[0]).to.contain.property('password');
                done();
            }).catch(done);
    })

    it('signing in a  user', (done) => {
        const data = {
            username: 'jon12334456456',
            password: 'jon123pass'
        }
        chai.request(app)
            .post('/v1/user/signIn')
            .send(data)
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                expect(body[0]).to.contain.property('firstName');
                expect(body[0]).to.contain.property('lastName');
                expect(body[0]).to.contain.property('username').eq('jon12334456456');
                expect(body[0]).to.contain.property('email');
                expect(body[0]).to.contain.property('password');
                done();
            }).catch(done);
    })

    it('deleting one user ', (done) => {
        const username = 'jon12334456456'
        chai.request(app)
            .delete('/v1/user/' + username)
            .then((res) => {
                expect(res).to.have.status(200);
                done();
            }).catch(done);
    })
})