process.env.NODE_ENV = 'test';
const expect = require('chai').expect;
const request = require('supertest');
const app = require('../../app');

const req = request(app);
const chai = require('chai')
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

describe('UserController', () => {

    it('creating a new device', (done) => {
        const username = 'rose123'
        const data = {
            devices: {
                deviceType: 'IPHONE',
                deviceModal: 'MAXXY',
                IMEI: 'hello123hello'
            }
        }
        chai.request(app)
            .post('/v1/device/' + username)
            .send(data)
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                expect(body).to.contain.property('username');
                expect(body).to.contain.property('devices');
                expect(body.devices).to.contain.property('deviceType');
                expect(body.devices).to.contain.property('deviceModal');
                expect(body.devices).to.contain.property('IMEI');
                done();
            }).catch(done);
    })


    it('getting all Devices ', (done) => {
        chai.request(app)
            .get('/v1/device')
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                expect(body).to.be.a('Array')
                expect(body[0]).to.contain.property('deviceType');
                expect(body[0]).to.contain.property('deviceModal');
                expect(body[0]).to.contain.property('IMEI');
                done();
            }).catch(done);
    })

    it('getting all UsersDevices ', (done) => {
        chai.request(app)
            .get('/v1/usersdevices')
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                expect(body).to.be.a('Array')
                expect(body[0]).to.contain.property('username');
                expect(body[0]).to.contain.property('devices');
                expect(body[0].devices).to.contain.property('deviceType');
                expect(body[0].devices).to.contain.property('deviceModal');
                expect(body[0].devices).to.contain.property('IMEI');
                done();
            }).catch(done);
    })


    it('getting one users devices ', (done) => {
        const username = 'rose123'
        chai.request(app)
            .get('/v1/usersdevices/find/' + username)
            .then((res) => {
                expect(res).to.have.status(200);
                const body = res.body;
                // expect(body[0]).to.contain.value('jon12334456456')
                expect(body[0]).to.contain.property('username');
                expect(body[0]).to.contain.property('devices');
                expect(body[0].devices).to.contain.property('deviceType');
                expect(body[0].devices).to.contain.property('deviceModal');
                expect(body[0].devices).to.contain.property('IMEI');
                done();
            }).catch(done);
    })

    it('deleting one device ', (done) => {
        const IMEI = 'hello123hello'
        chai.request(app)
            .delete('/v1/device/delete/' + IMEI)
            .then((res) => {
                expect(res).to.have.status(200);
                done();
            }).catch(done);
    })
})