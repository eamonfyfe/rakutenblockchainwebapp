const Joi = require('joi')
  const Device = require('../models/DeviceModal')
  const UserAndDevice = require('../models/UserAndDeviceModal')
  const bodyParser = require('body-parser').json()

  function init(app) {
    app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*')
      res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
      next()
    })
  // Get All Devices
  app.get('/v1/device', (req, res) => {
    Device.find()
      .exec()
      .then(docs => {
        if (docs.length >= 0) {
          res.status(200).json(docs)
        } else {
          res.status(404).json({
            message: 'Add data to users!'
          })
        }
      })
      .catch(err => {
        console.log(err)
        res.status(500).json({
          error: err
        })
      })
  })


  app.get('/v1/usersdevices', (req, res) => {
    UserAndDevice.find()
        .exec()
        .then(docs => {
          if (docs.length >= 0) {
            res.status(200).json(docs)
          } else {
            res.status(404).json({
              message: 'Add data to users!'
            })
          }
        })
        .catch(err => {
          console.log(err)
          res.status(500).json({
            error: err
          })
        })
  })
  
  // Get devices for the user
  app.get('/v1/usersdevices/find/:username', (req, res) => {
    const username = req.params.username
    UserAndDevice.find({username})
      .exec()
      .then(docs => {
        if (docs.length >= 0) {
          res.status(200).json(docs)
        } else {
          res.status(404).json({
            message: 'No devices under this username!'
          })
        }
      })
      .catch(err => {
        console.log(err)
        res.status(500).json({
          error: err
        })
      })
  })


  //update device to be added to the chain
  app.put('/v1/usersdevices/updateChainStatus/:IMEI', (req, res) => {
    const username = req.params.username
    const IMEI = req.params.IMEI

    UserAndDevice.findOneAndUpdate({'devices.IMEI': IMEI}, req.body).then(function() {
    UserAndDevice.findOne({'devices.IMEI': IMEI}).exec().then(result => {
        res.status(200).json(result)
      }).catch(err => {
        console.log(err)
        res.status(500).json(err)
      })
    })
  })
  // Find
  app.get('/v1/device/:IMEI', (req, res) => {
    const imei = req.params.IMEI
    Device.find({imei})
      .exec()
      .then(doc => {
        if (doc) {
          res.status(200).json(doc)
        } else {
          res.status(404).json({
            message: 'No valid entry'
          })
        }
      }).catch(err => {
        console.log(err)
        res.status(500).json({error: err})
      })
  })
  
  app.post('/v1/device/create', (req, res) => {
      const newDevice = new Device({
        deviceType: req.body.deviceType,
        deviceModal: req.body.deviceModal,
        IMEI: req.body.IMEI
      })
      newDevice.save().then(result => {
        console.log(result)
      })
      res.send(newDevice)
    //}
  })

    app.post('/v1/device/:username', bodyParser, (req, res) => {
      const username = req.params.username
      let deviceErrors = [];
      let deviceType = req.body.devices.deviceType;
      let deviceModal = req.body.devices.deviceModal;
      let IMEI = req.body.devices.IMEI;
      UserAndDevice.findOne({'devices.IMEI': IMEI}).exec().then(result => {
        if(result) {
          deviceErrors.push({ msg: 'Sorry this device is already signed up!' })
        }
      if (!deviceType || !deviceModal || !IMEI) {
        deviceErrors.push({ msg: 'Please Fill in all fields' })
      }
      if (req.body.devices.IMEI.length <= 9) {
        deviceErrors.push({ msg: 'IMEI must be 10 Characters' })
      } if (deviceErrors.length > 0) {
        res.status(400).json({error: deviceErrors})
      }
      else {
        const newDevice = new UserAndDevice({
          username: username,
          devices: req.body.devices,
          addedToChain: req.body.addedToChain
        })
        newDevice.save().then(result => {
          console.log(result)
          res.status(200).json(result, {
            message: 'User created!'
          })
        }).catch(err => {
          console.log(err)
          res.status(500).json({
            error: err
          })
        })
        res.send(newDevice)
        res.end()
      }
      })
    })

  
  app.delete('/v1/device/delete/:IMEI', (req, res) => {
    const IMEI = req.params.IMEI
    UserAndDevice.remove({ 'devices.IMEI': IMEI }).exec().then(result => {
      res.status(200).json(result)
    }).catch(err => {
      console.log(err)
      res.status(500).json({
        error: err
      })
    })
  })
}

function validateDevice (device) {
  const schema = {
    deviceType: Joi.string().min(3).required(),
    deviceModal: Joi.string().required(),
    IMEI: Joi.string().min(10).required()
  }

  return Joi.validate(device, schema)
}

module.exports = { init };
