const Joi = require('joi')
const TransactionReceipt = require('../models/TransactionReceiptModal')
const UserAndDevice = require('../models/UserAndDeviceModal')
const bodyParser = require('body-parser').json()

function init(app) {
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
        next()
    })
    // Get All Devices
    app.get('/v1/transactionReceipt', (req, res) => {
        TransactionReceipt.find()
            .exec()
            .then(docs => {
                if (docs.length >= 0) {
                    res.status(200).json(docs)
                } else {
                    res.status(404).json({
                        message: 'Add data to users!'
                    })
                }
            })
            .catch(err => {
                console.log(err)
                res.status(500).json({
                    error: err
                })
            })
    })

    app.get('/v1/transactionReceipt/:username/:IMEI', (req, res) => {
        const username = req.params.username
        const IMEI = req.params.IMEI
        TransactionReceipt.findOne({'devices.IMEI': IMEI, 'username' : username}).exec()
            .then(doc => {
                if (doc) {
                    res.status(200).json(doc)
                } else {
                    res.status(404).json({
                        message: 'No valid entry'
                    })
                }
            }).catch(err => {
            console.log(err)
            res.status(500).json({error: err})
        })
    })

    app.post('/v1/transactionReceipt/create', bodyParser, (req, res) => {
        const username =  req.body.username
        const newReceipt = new TransactionReceipt({
            username: username,
            devices: req.body.devices,
            transactionHash: req.body.transactionHash,
            blockHash: req.body.blockHash,
            blockNumber: req.body.blockNumber,
            destination: req.body.destination,

        })
        newReceipt.save().then(result => {
            res.status(200).json(result, {
                message: 'New Receipt Added'
            })
        }).catch(err => {
            console.log(err)
            res.status(500).json({
                error: err
            })
        })
        res.send(newReceipt)
        res.end()
    })

}

module.exports = { init };
