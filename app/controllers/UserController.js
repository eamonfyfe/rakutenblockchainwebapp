const Joi = require('joi')
const User = require('../models/UserModel')
const users = []
const bcrypt = require('bcryptjs');
const saltRounds = 10;

function init(app) {
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type');
    next()
  })
  // Get All Users
  app.get('/v1/user', (req, res) => {
    User.find()
          .exec()
          .then(docs => {
            if (docs.length >= 0) {
              res.status(200).json(docs)
            } else {
              res.status(404).json({
                message: 'Add data to users!'
              })
            }
          })
          .catch(err => {
            console.log(err)
            res.status(500).json({
              error: err
            })
          })
  })

  // Find
  app.get('/v1/user/:username', (req, res) => {
    const username = req.params.username
    User.find({username})
          .exec()
          .then(doc => {
            if (doc) {
              res.status(200).json(doc)
            } else {
              res.status(404).json({
                message: 'No valid entry'
              })
            }
          }).catch(err => {
            console.log(err)
            res.status(500).json({error: err})
          })
  })

  // Create
  app.post('/v1/user/create', (req, res) => {
        let userErrors = [];
        let firstName = req.body.firstName;
        let lastName = req.body.lastName;
        let username = req.body.username;
        let email = req.body.email;
        let password =  req.body.password;
        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

        User.findOne({'username': username}).exec().then(result => {
          if (result) {
            userErrors.push({msg: 'Sorry this user is already signed up!'})
          }
          if (!firstName || !lastName || !username || !email || !password) {
            userErrors.push({msg: 'Please Fill in all fields'})
          }
          if (!email.match(mailformat)) {
            userErrors.push({msg: 'Please enter a valid email!'})
          }
          if (username.length <= 4) {
            userErrors.push({msg: 'Username must be at least 4 Characters'})
          }
          if (password.length <= 4) {
            userErrors.push({msg: 'Password must be at least 4 Characters'})
          }
          if (userErrors.length > 0) {
            res.status(400).json({error: userErrors})
          } else {
            bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
              // Store hash in your password DB.
              const newUser = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                username: req.body.username,
                email: req.body.email,
                password: hash,
                devices: req.body.devices
              })
              newUser.save().then(result => {
                res.status(201).json(result, {
                  message: 'User created!'
                })
              }).catch(err => {
                console.log(err)
                res.status(500).json({
                  error: err
                })
              })
              users.push(newUser)
              res.send(newUser)
              res.end()
            });
          }
        })
      })

  // Update
  app.patch('/v1/user/:username', (req, res) => {
    const username = req.params.username
    const updateOps = {}
    for (const ops of req.body) {
      updateOps[ops.propName] = ops.value
    }
    User.update({username: username}, {
      $set: updateOps
    }).exec().then(result => {
      res.status(200).json(result)
    }).catch(err => {
      console.log(err)
      res.status(500).json(err)
    })
  })

  // Delete
  app.delete('/v1/user/:username', (req, res) => {
    const username = req.params.username
    User.remove({username: username}).exec().then(result => {
      res.status(200).json(result)
    }).catch(err => {
      console.log(err)
      res.status(500).json({
        error: err
      })
    })
  })

  // Sign In
  app.post('/v1/user/signIn', (req, res) => {
    const {body} = req
    const {
          username,
          password
      } = body

    let loginErrors = [];

    User.find({
      username: username}, async (err, users)  => {
      if (err) {
        loginErrors.push({ msg: 'Sorry there was an error please try again' })
      }
      if (users.length !== 1) {
        loginErrors.push({ msg: 'Sorry there is no user with that username on our system' })
        res.status(400).json({error: loginErrors})
      }
      const user = users[0]
      const match = await bcrypt.compare(body.password, user.password)
      if (match === false) {
        loginErrors.push({ msg: 'Invalid Password!' })
      }
      if (loginErrors.length > 0) {
        res.status(400).json({error: loginErrors})
      }
      else {
        User.find({
          username: username})
            .exec()
            .then(doc => {
              if (doc) {
                res.status(200).json(doc)
              } else {
                res.status(404).json({error: 'Try again'})
              }
            }).catch(err => {
          res.status(500).json({error: err})
        })
      }
      })
    })

  // Sign Up
  app.post('/v1/user/signUp', (req, res) => {
    const {body} = req
    const {
          firstName,
          lastName,
          username,
          email,
          password
      } = body

    if (!firstName) {
      return res.send({
        success: false,
        message: 'Error: First Name cant be blank'
      })
    }

    if (!lastName) {
      return res.send({
        success: false,
        message: 'Error: Last Name cant be blank'
      })
    }

    if (!username) {
      return res.send({
        success: false,
        message: 'Error: Username cant be blank'
      })
    }
    if (!email) {
      return res.send({
        success: false,
        message: 'Error: Email cant be blank'
      })
    }

    if (!password) {
      return res.send({
        success: false,
        message: 'Error: Password cant be blank'
      })
    }

    body.email = email.toLowerCase()
    User.find({
      email: email
    }, (err, previousUsers) => {
      if (err) {
        return res.send({
          success: false,
          message: 'Error: Server Error'
        })
      } else if (previousUsers.length > 0) {
        return res.send({
          success: false,
          message: 'Error: Account Already Exists'
        })
      }
    })

    const newUser = User()
    newUser.email = email
    newUser.firstName = firstName
    newUser.username = username
    newUser.lastName = lastName
    newUser.password = newUser.generateHash(password)
    newUser.save((err, user) => {
      if (err) {
        return res.send({
          success: false,
          message: 'Error: Server Error'
        })
      } else {
        return res.send({
          success: true,
          message: 'Signed Up!'
        })
      }
    })
  })
}

function validateUser (user) {
  const schema = {
    firstName: Joi.string().min(3).required(),
    lastName: Joi.string().min(3).required(),
    username: Joi.string().min(3).required(),
    email: Joi.string().email().required(),
    password: Joi.string().min(3).required(),
    devices: Joi.array()
  }

  return Joi.validate(user, schema)
}
module.exports = { init };
