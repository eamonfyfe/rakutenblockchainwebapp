import React from 'react'

function AllDevices () {
  return (
    <article>
      <section className='text-section'>
        <h1>Your Device has been added!</h1>
        <p>
          Please call in to one of our stores below to check in your device!
        </p>
      </section>
    </article>
  )
}

export default AllDevices
