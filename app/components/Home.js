import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link} from "react-router";

class Home extends Component {
  render () {
    return (
      <article>
        <div>
          <section className='text-section'>
            <h1>Welcome to MyDevice!</h1>
            <p>This is the app where you get to register your device to the Blockchain where it is recognised as your!</p>

            <p>All you have to do is sign up and follow along to check out where you device will be stored! </p>
            <p>Please head to the login page to get through or else sign up now</p>
          </section>
        </div>
      </article>
    )
  }
}

function select (state) {
  return {
    data: state
  }
}

export default connect(select)(Home)
