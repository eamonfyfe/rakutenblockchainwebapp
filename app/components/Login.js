import React, {Component} from 'react'
import {connect} from 'react-redux'
import LoginForm from './common/LoginForm'

import {clearError, loginRequest} from '../actions'
import {browserHistory} from "react-router";
const fetch = require('node-fetch')

class Login extends Component {
  constructor (props) {
    super(props)

    this._login = this._login.bind(this)
      this._clearError = this._clearError.bind(this)
      this.state = {
          userLoginErrors: ''
      }
  }

  render () {
    const { dispatch } = this.props
    const { formState, currentlySending, error } = this.props.data

    return (
      <div className='form-page__wrapper'>
        <div className='form-page__form-wrapper'>
          <div className='form-page__form-header'>
            <h2 className='form-page__form-heading'>Login</h2>
          </div>
          <LoginForm data={formState} dispatch={dispatch} history={this.props.history} onSubmit={this._login}
            btnText={'Login'} error={this.state.userLoginErrors} currentlySending={currentlySending} />
        </div>
      </div>
    )
  }

  _login (username, password) {
      this._clearError()
    fetch('http://localhost:3007/v1/user/signIn' , {
      method: 'POST',
        body: JSON.stringify({
            username: username,
            password: password
        }),
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Accept': 'application/json'
        }
    })
        .then(res => res.json())
        .then(json => {
            if (json.error) {
                const errorMessage = json.error[0].msg;
                this.setState({userLoginErrors: errorMessage})
            } else {
                window.localStorage.setItem('userLoggedIn', 'true')
                forwardTo('/dashboard')
            }
        })
    fetch('http://localhost:3007/v1/user/' + encodeURIComponent(username), {
      method: 'GET',
      header: ''
    })
      .then(res => res.json())
      .then(json => {
        window.localStorage.setItem('firstName', json[0].firstName)
        window.localStorage.setItem('lastName', json[0].lastName)
        window.localStorage.setItem('username', json[0].username)
        window.localStorage.setItem('email', json[0].email)
      })
  }

    _clearError () {
        this.props.dispatch(clearError())
    }
}

Login.propTypes = {
  data: React.PropTypes.object,
  history: React.PropTypes.object,
  dispatch: React.PropTypes.func
}

function select (state) {
  return {
    data: state
  }
}

function forwardTo (location) {
    browserHistory.push(location)
}

export default connect(select)(Login)
