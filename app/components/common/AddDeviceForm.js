import React, {Component} from 'react'
import ErrorMessage from './ErrorMessage'
import LoadingButton from './LoadingButton'

import {changeForm} from '../../actions'

class AddDeviceForm extends Component {
  constructor (props) {
    super(props)

    this._onSubmit = this._onSubmit.bind(this)
    this._changeDeviceType = this._changeDeviceType.bind(this)
    this._changeIMEI = this._changeIMEI.bind(this)
    this._changeDeviceModal = this._changeDeviceModal.bind(this)
    this._changeUsername = this._changeUsername.bind(this)
  }

  render () {
    const {error} = this.props

    return (
      <form className='form' onSubmit={this._onSubmit}>
        {error ? <ErrorMessage error={error} /> : null}
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            id='deviceType'
            value={this.props.data.deviceType}
            placeholder='Type of Device'
            onChange={this._changeDeviceType}
            autoCorrect='off'
            autoCapitalize='off'
            spellCheck='false' />
          <label className='form__field-label' htmlFor='deviceType'>
            Type of Device
          </label>
        </div>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            id='deviceModal'
            value={this.props.data.deviceModal}
            placeholder='Device Modal'
            onChange={this._changeDeviceModal}
            autoCorrect='off'
            autoCapitalize='off'
            spellCheck='false' />
          <label className='form__field-label' htmlFor='deviceModal'>
            Device Modal
          </label>
        </div>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            id='IMEI'
            value={this.props.data.IMEI}
            placeholder='AA-BBBBBB-CCCCCC-D: AA'
            onChange={this._changeIMEI}
            autoCorrect='off'
            autoCapitalize='off'
            spellCheck='false' />
          <label className='form__field-label' htmlFor='IMEI'>
            IMEI
          </label>
        </div>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            defaultValue={window.localStorage.getItem('username')}
            readOnly={'Hello'}
          />
          <label className='form__field-label' htmlFor='deviceType'>
            Username
          </label>
        </div>
        <div className='form__submit-btn-wrapper'>
          {this.props.currentlySending ? (
            <LoadingButton />
          ) : (
            <button className='form__submit-btn' type='submit'>
              {this.props.btnText}
            </button>
          )}
        </div>
      </form>
    )
  }
  _changeDeviceType (event) {
    this._emitChange({...this.props.data, deviceType: event.target.value})
  }

  _changeIMEI (event) {
    this._emitChange({...this.props.data, IMEI: event.target.value})
  }

  _changeDeviceModal (event) {
    this._emitChange({...this.props.data, deviceModal: event.target.value})
  }

  _changeUsername (event) {
    this._emitChange({...this.props.data, username: event.target.value})
  }

  _emitChange (newFormState) {
    this.props.dispatch(changeForm(newFormState))
  }

  _onSubmit (event) {
    event.preventDefault()
    this.props.onSubmit(this.props.data.deviceType, this.props.data.IMEI, this.props.data.deviceModal)
  }
}

AddDeviceForm.propTypes = {
  dispatch: React.PropTypes.func,
  data: React.PropTypes.object,
  onSubmit: React.PropTypes.func,
  changeForm: React.PropTypes.func,
  btnText: React.PropTypes.string,
  error: React.PropTypes.string,
  currentlySending: React.PropTypes.bool
}

export default AddDeviceForm
