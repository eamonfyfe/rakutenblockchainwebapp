import React, {Component} from 'react'

class DeviceDetailsForm1 extends Component {
  constructor (props) {
    super(props)

    this._onSubmit = this._onSubmit.bind(this)
  }
  render () {
    return (
      <form className='form' onSubmit={this._onSubmit}>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            defaultValue={window.localStorage.getItem('deviceType4')}
            readOnly={'Hello'}
          />
          <label className='form__field-label' htmlFor='deviceType'>
            Device Type
          </label>
        </div>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            defaultValue={window.localStorage.getItem('deviceModal4')}
            readOnly={'Hello'}
          />
          <label className='form__field-label' htmlFor='deviceType'>
            Device Modal
          </label>
        </div>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            defaultValue={window.localStorage.getItem('IMEI4')}
            readOnly={'Hello'}
          />
          <label className='form__field-label' htmlFor='deviceType'>
            IMEI
          </label>
        </div>
        <div className='form__field-wrapper'>
          <input
              className='form__field-input'
              type='text'
              defaultValue={window.localStorage.getItem('addedToChain4')}
              readOnly={'Hello'}
          />
          <label className='form__field-label' htmlFor='addedToChain'>
            Is Your Device on the Chain?
          </label>
        </div>
      </form>
    )
  }

  _onSubmit (event) {
    event.preventDefault()
    this.props.onSubmit(this.props.data.firstName)
  }
}

DeviceDetailsForm1.propTypes = {
  dispatch: React.PropTypes.func,
  data: React.PropTypes.object,
  onSubmit: React.PropTypes.func,
  changeForm: React.PropTypes.func,
  btnText: React.PropTypes.string,
  error: React.PropTypes.string,
  currentlySending: React.PropTypes.bool
}

export default DeviceDetailsForm1
