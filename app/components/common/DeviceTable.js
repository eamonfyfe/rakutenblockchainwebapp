import React, { Component, } from 'react'
const fetch = require('node-fetch')
const userData = []
import 'bootstrap/dist/css/bootstrap.min.css'
import {Link} from "react-router";

class DeviceTable extends Component {

  constructor (props) {
    super(props)

    this.state = {
      usersData: []
    }
  }

  componentWillMount () {
    this.renderMyData()
  }

  renderMyData () {
      fetch('http://localhost:3007/v1/usersdevices', {
          method: 'GET'
      })
          .then(res => res.json())
          .then(json => {
              userData.push(json)
              this.setState({usersData: userData[0]})
          })
      return this.state.usersData.map((user, index) => {
              return (
                  <tr key={user.devices.IMEI}>
                      <td>{user.username}</td>
                      <td>{user.devices.deviceType}</td>
                      <td>{user.devices.deviceModal}</td>
                      <td>{user.devices.IMEI}</td>
                  </tr>
              )
          })
  }

  render () {
    return (
        <div>
        <table className="table">
            <thead>
            <tr>
                <th>USERNAME</th>
                <th>DEVICE TYPE</th>
                <th>DEVICE MODAL</th>
                <th>IMEI</th>
            </tr>
            </thead>
            <tbody>
            {this.renderMyData()}
            </tbody>
        </table>
            <Link to='localhost:3000' className='form__submit-btn'>
                Add User Device to the Chain
            </Link>
        </div>

    )
  }
}
export default DeviceTable