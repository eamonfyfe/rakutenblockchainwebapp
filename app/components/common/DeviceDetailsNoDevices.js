import React from 'react'

function NoDevices () {
  return (
    <article>
      <section className='text-section'>
        <h1>You currently have no devices added!</h1>
        <p>
          Head to add Device now to add your device to the blockchain :)
        </p>
      </section>
    </article>
  )
}

export default NoDevices
