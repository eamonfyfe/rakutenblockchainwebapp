import React from 'react'
import GoogleMap from '../GoogleMap'

function DeviceAdded () {
  return (
    <article>
      <section className='text-section'>
        <h1>Next Steps: </h1>
        <p>
          Please call in to one of our stores below to check in your device!
        </p>
        <GoogleMap />
      </section>
    </article>
  )
}

export default DeviceAdded
