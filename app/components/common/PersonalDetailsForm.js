import React, {Component} from 'react'

class PersonalDetailsForm extends Component {
  constructor (props) {
    super(props)

    this._onSubmit = this._onSubmit.bind(this)
  }
  render () {
    return (
      <form className='form' onSubmit={this._onSubmit}>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            defaultValue={window.localStorage.getItem('firstName')}
            readOnly={'Hello'}
          />
          <label className='form__field-label' htmlFor='deviceType'>
          First Name
          </label>
        </div>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            defaultValue={window.localStorage.getItem('lastName')}
            readOnly={'Hello'}
          />
          <label className='form__field-label' htmlFor='deviceType'>
            Last Name
          </label>
        </div>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            defaultValue={window.localStorage.getItem('username')}
            readOnly={'Hello'}
          />
          <label className='form__field-label' htmlFor='deviceType'>
            Username
          </label>
        </div>
        <div className='form__field-wrapper'>
          <input
            className='form__field-input'
            type='text'
            defaultValue={window.localStorage.getItem('email')}
            readOnly={'Hello'}
          />
          <label className='form__field-label' htmlFor='deviceType'>
            Email
          </label>
        </div>
      </form>
    )
  }

  _onSubmit (event) {
    event.preventDefault()
  }
}

PersonalDetailsForm.propTypes = {
  dispatch: React.PropTypes.func,
  data: React.PropTypes.object,
  onSubmit: React.PropTypes.func,
  changeForm: React.PropTypes.func,
  btnText: React.PropTypes.string,
  error: React.PropTypes.string,
  currentlySending: React.PropTypes.bool
}

export default PersonalDetailsForm
