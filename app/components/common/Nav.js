import React, {Component} from 'react'
import LoadingButton from './LoadingButton'
import { browserHistory, Link } from 'react-router'

import {logout, clearError} from '../../actions'
const fetch = require('node-fetch')
export const userData = []

class Nav extends Component {
  constructor (props) {
    super(props)
    this._logout = this._logout.bind(this)
    this._clearError = this._clearError.bind(this)
    this._getPersonalDetails = this._getPersonalDetails.bind(this)
    this._getDeviceDetails = this._getDeviceDetails.bind(this)
    this._getAllData = this._getAllData.bind(this)
  }

  render () {
    const AdminLoggedIn = window.localStorage.getItem('AdminLoggedIn')
      const UserLoggedIn = window.localStorage.getItem('userLoggedIn')

    const navButtons = UserLoggedIn ? (
      <div>
        <Link to='/dashboard' className='btn btn--dash btn--nav' onClick={this._getAllData}>Dashboard</Link>
        <Link to='/personalDetails' className='btn btn--dash btn--nav' onClick={this._getPersonalDetails} >Personal Details</Link>
        <Link to='/devicesDetails' className='btn btn--dash btn--nav' onClick={this._getDeviceDetails}>Devices' Details</Link>
        <Link to='/AddDevice' className='btn btn--dash btn--nav'>Add Device</Link>
        {this.props.currentlySending ? (
          <LoadingButton className='btn--nav' />
        ) : (
          <a href='#' className='btn btn--login btn--nav' onClick={this._logout}>Logout</a>
        )}
      </div>
    ) : (
      <div>
        <Link to='/register' className='btn btn--login btn--nav' onClick={this._clearError}>Register</Link>
        <Link to='/login' className='btn btn--login btn--nav' onClick={this._clearError}>Login</Link>
        <Link to='/adminLogin' className='btn btn--login btn--nav' onClick={this._clearError} onlyActiveOnIndex>Admin Login</Link>
      </div>
    )

    return (
      <div className='nav'>
        {AdminLoggedIn
          ? <div className='nav__wrapper'>
            <Link to='/dashboard' className='nav__logo-wrapper' onClick={this._clearError}>
              <h1 className='nav__logo'>Declare Device</h1>
            </Link>
            <Link to='/AdminDashboard' className='btn btn--login btn--nav' onClick={this._clearError}>Admin Dashboard</Link>
            <a href='#' className='btn btn--login btn--nav' onClick={this._logout}>Logout</a>
          </div>
          : <div className='nav__wrapper'>
            <Link to='/dashboard' className='nav__logo-wrapper' onClick={this._clearError}>
              <h1 className='nav__logo'>Declare Device</h1>
            </Link>
            {navButtons}
          </div>
        }
      </div>
    )
  }

  _logout () {
      window.localStorage.removeItem('userLoggedIn')
      window.localStorage.removeItem('AdminLoggedIn')
      window.localStorage.removeItem('device1')
      window.localStorage.removeItem('deviceType1')
      window.localStorage.removeItem('deviceModal1')
      window.localStorage.removeItem('IMEI1')
      window.localStorage.removeItem('addedToChain1')
      window.localStorage.removeItem('device2')
      window.localStorage.removeItem('deviceType2')
      window.localStorage.removeItem('deviceModal2')
      window.localStorage.removeItem('IMEI2')
      window.localStorage.removeItem('addedToChain2')
      window.localStorage.removeItem('device3')
      window.localStorage.removeItem('deviceType3')
      window.localStorage.removeItem('deviceModal3')
      window.localStorage.removeItem('IMEI3')
      window.localStorage.removeItem('addedToChain3')
      window.localStorage.removeItem('device4')
      window.localStorage.removeItem('deviceType4')
      window.localStorage.removeItem('deviceModal4')
      window.localStorage.removeItem('IMEI4')
      window.localStorage.removeItem('addedToChain4')
    this.props.dispatch(logout())
  }

  _getPersonalDetails () {
    this._clearError()
    const username = window.localStorage.getItem('username')
      fetch('http://localhost:3007/v1/user/' + encodeURIComponent(username), {
          method: 'GET',
          header: ''
      })
          .then(res => res.json())
          .then(json => {
              window.localStorage.setItem('firstName', json[0].firstName)
              window.localStorage.setItem('lastName', json[0].lastName)
              window.localStorage.setItem('email', json[0].email)
          }).then(forwardTo('/personalDetails'))
  }

  _getDeviceDetails () {
    this._clearError()
    const username = window.localStorage.getItem('username')
    fetch('http://localhost:3007/v1/usersdevices/find/' + username, {
      method: 'GET'
    })
      .then(res => res.json())
      .then(json => {
        window.localStorage.setItem('device1', json[0].devices)
        window.localStorage.setItem('deviceType1', json[0].devices.deviceType)
        window.localStorage.setItem('deviceModal1', json[0].devices.deviceModal)
        window.localStorage.setItem('IMEI1', json[0].devices.IMEI)
        window.localStorage.setItem('addedToChain1', json[0].addedToChain === true ? 'Yes' : 'No')
        window.localStorage.setItem('device2', json[1].devices)
        window.localStorage.setItem('deviceType2', json[1].devices.deviceType)
        window.localStorage.setItem('deviceModal2', json[1].devices.deviceModal)
        window.localStorage.setItem('IMEI2', json[1].devices.IMEI)
        window.localStorage.setItem('addedToChain2', json[1].addedToChain === true ? 'Yes' : 'No')
        window.localStorage.setItem('device3', json[2].devices)
        window.localStorage.setItem('deviceType3', json[2].devices.deviceType)
        window.localStorage.setItem('deviceModal3', json[2].devices.deviceModal)
        window.localStorage.setItem('IMEI3', json[2].devices.IMEI)
        window.localStorage.setItem('addedToChain3', json[2].addedToChain === true ? 'Yes' : 'No')
        window.localStorage.setItem('device4', json[3].devices)
        window.localStorage.setItem('deviceType4', json[3].devices.deviceType)
        window.localStorage.setItem('deviceModal4', json[3].devices.deviceModal)
        window.localStorage.setItem('IMEI4', json[3].devices.IMEI)
        window.localStorage.setItem('addedToChain4', json[3].addedToChain === true ? 'Yes' : 'No')
      }).then(forwardTo('/devicesDetails'))
  }
  _getAllData () {
    fetch('http://localhost:3007/v1/device/all', {
      method: 'GET'
    })
      .then(res => res.json())
      .then(json => {
        console.log(json)
        userData.push(json)
      })
  }

  _clearError () {
    this.props.dispatch(clearError())
  }
}

function forwardTo (location) {
  browserHistory.push(location)
}

Nav.propTypes = {
  loggedIn: React.PropTypes.bool,
  currentlySending: React.PropTypes.bool,
  dispatch: React.PropTypes.func
}

export default Nav
