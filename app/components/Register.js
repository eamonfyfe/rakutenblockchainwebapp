import React, {Component} from 'react'
import {connect} from 'react-redux'
import { registerRequest } from '../actions'
import RegisterForm from './common/RegisterForm'
import {browserHistory} from "react-router";
const fetch = require('node-fetch')

class Register extends Component {
  constructor (props) {
    super(props)

    this._register = this._register.bind(this)
    this.state = {
      userErrors: ''
    }
  }

  render () {
    const {dispatch} = this.props
    const {formState, currentlySending, error} = this.props.data

    return (
      <div className='form-page__wrapper'>
        <div className='form-page__form-wrapper'>
          <div className='form-page__form-header'>
            <h2 className='form-page__form-heading'>Register</h2>
          </div>
          <RegisterForm data={formState} dispatch={dispatch} history={this.props.history} onSubmit={this._register} btnText={'Register'} error={this.state.userErrors} currentlySending={currentlySending} />
        </div>
      </div>
    )
  }

  _register (firstName, lastName, username, email, password) {
    window.localStorage.setItem('username', username)
    //this.props.dispatch(registerRequest({firstName, lastName, username, email, password}))
    fetch('http://localhost:3007/v1/user/create', {
      method: 'POST',
      body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        username: username,
        email: email,
        password: password
      }),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }})
      .then(res => res.json())
      .then(json => {
        if (json.error) {
          const errorMessage = json.error[0].msg;
          this.setState({userErrors: errorMessage})
        } else {
          forwardTo('/login')
        }
      })
  }
}

Register.propTypes = {
  data: React.PropTypes.object,
  history: React.PropTypes.object,
  dispatch: React.PropTypes.func
}

function select (state) {
  return {
    data: state
  }
}
function forwardTo (location) {
  browserHistory.push(location)
}

export default connect(select)(Register)
