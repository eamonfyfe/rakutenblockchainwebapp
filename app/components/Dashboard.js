import React from 'react'
import GoogleMap from './GoogleMap'

function Dashboard () {
  return (
    <article>
        <section className='text-section'>
          <h1>Dashboard</h1>
          <h2> What is Declare Device</h2>
          <p> At Declare Device we want to make sure what is yours stay yours! There are a lot of people that want to
            get their hand on your gadgets and its our job to prove that these are yours!
            We are here to help you tell the world that your devices are yours and that no one has the right to take
            them from you. Sign up today to make sure that you are made sure that no crook can take whats yours!! </p>
          <h2> Why use Declare Device?</h2>
          <p> We are here to help you ensure that your device stays yours. Us guys at Declare Device dont like it when
            your gadget is stolen and the thief completely takes over your phone and declare it is theirs!
            So we are here to help, by adding your device to the blockchain it is made safe to the how world that your
            device is registered as yours and no one can change that by how safe the blockchain is!</p>
          <h2> What is the process?</h2>
          <p>First of all start by signing up to Declare Device.
            Next add your device details to the system at our Add Device page
            After that is done make sure to check out the locations of all our store and find the closest one to you.
            Pop into our store with your device to verify that its yours and we will add it to the blockchain.
            And that's it your device is safe in our hands!
          </p>
          <h2> Where can i register my device?</h2>
          <p> Simple! After we have grabbed your details come into one of the following locations below!</p>
          <GoogleMap />
        </section>
    </article>
  )
}

export default Dashboard
