import React, {Component} from 'react'
import {connect} from 'react-redux'

import PersonalDetailsForm from './common/PersonalDetailsForm'

class PersonalDetails extends Component {
  render () {
    window.localStorage.removeItem('device1')
    window.localStorage.removeItem('deviceType1')
    window.localStorage.removeItem('deviceModal1')
    window.localStorage.removeItem('IMEI1')
    window.localStorage.removeItem('addedToChain1')
    window.localStorage.removeItem('device2')
    window.localStorage.removeItem('deviceType2')
    window.localStorage.removeItem('deviceModal2')
    window.localStorage.removeItem('IMEI2')
    window.localStorage.removeItem('addedToChain2')
    window.localStorage.removeItem('device3')
    window.localStorage.removeItem('deviceType3')
    window.localStorage.removeItem('deviceModal3')
    window.localStorage.removeItem('IMEI3')
    window.localStorage.removeItem('addedToChain3')
    window.localStorage.removeItem('device4')
    window.localStorage.removeItem('deviceType4')
    window.localStorage.removeItem('deviceModal4')
    window.localStorage.removeItem('IMEI4')
    window.localStorage.removeItem('addedToChain4')
    const {dispatch} = this.props
    const {formState, currentlySending, error} = this.props.data
    return (
      <div className='form-page__wrapper'>
        <div className='form-page__form-wrapper'>
          <div className='form-page__form-header'>
            <h2 className='form-page__form-heading'>Your Personal Details</h2>
          </div>
          <PersonalDetailsForm data={formState} dispatch={dispatch} history={this.props.history} btnText={'Update'} error={error} currentlySending={currentlySending} />
        </div>
      </div>
    )
  }
}

PersonalDetails.propTypes = {
  data: React.PropTypes.object,
  history: React.PropTypes.object,
  dispatch: React.PropTypes.func
}

// Which props do we want to inject, given the global state?
function select (state) {
  return {
    data: state
  }
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(PersonalDetails)
