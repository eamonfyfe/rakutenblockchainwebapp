import React, {Component} from 'react'
import {connect} from 'react-redux'
import AdminForm from './common/AdminForm'

import {loginRequest} from '../actions'
import { browserHistory } from 'react-router'
const fetch = require('node-fetch')

class AdminLogin extends Component {
  constructor (props) {
    super(props)

    this._adminLogin = this._adminLogin.bind(this)
    this.state = {
      adminLoginErrors: ''
    }
  }

  render () {
    const { dispatch } = this.props
    const { formState, currentlySending, error } = this.props.data
    return (
        <div className='form-page__wrapper'>
          <div className='form-page__form-wrapper'>
            <div className='form-page__form-header'>
              <h2 className='form-page__form-heading'>Admin Login</h2>
            </div>
            <AdminForm data={formState} dispatch={dispatch} history={this.props.history} onSubmit={this._adminLogin} btnText={'Login'} error={this.state.adminLoginErrors} currentlySending={currentlySending} />
          </div>
        </div>
    )
  }
  _adminLogin (adminUsername, password) {
    //this.props.dispatch(loginRequest({ adminUsername, password }))
    fetch('http://localhost:3007/v1/adminuser/signIn', {
      method: 'POST',
      body: JSON.stringify({
        username: adminUsername,
        password: password
      }),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    })
        .then(res => res.json())
        .then(json => {
          if (json.error) {
            const errorMessage = json.error[0].msg;
            this.setState({adminLoginErrors: errorMessage})
          } else {
            window.localStorage.setItem('AdminLoggedIn', 'true')
            forwardTo('/adminDashboard')
          }
        })
  }
}

AdminLogin.propTypes = {
  data: React.PropTypes.object,
  history: React.PropTypes.object,
  dispatch: React.PropTypes.func
}

function forwardTo (location) {
  browserHistory.push(location)
}

// Which props do we want to inject, given the global state?
function select (state) {
  return {
    data: state
  }
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(AdminLogin)
