import React, { Component } from 'react'
import { connect } from 'react-redux'
import NoDevices from './common/DeviceDetailsNoDevices'
import DeviceDetailsForm1 from './common/DeviceDetailsForm1'
import DeviceDetailsForm2 from './common/DeviceDetailsForm2'
import DeviceDetailsForm3 from './common/DeviceDetailsForm3'
import DeviceDetailsForm4 from './common/DeviceDetailsForm4'

class DeviceDetails extends Component {
  render () {
    const { dispatch } = this.props
    const username = window.localStorage.getItem('username')
    const { formState, currentlySending, error } = this.props.data
    fetch('http://localhost:3007/v1/usersdevices/find/' + username, {
      method: 'GET'
    })
        .then(res => res.json())
        .then(json => {
          window.localStorage.setItem('device1', json[0].devices)
          window.localStorage.setItem('deviceType1', json[0].devices.deviceType)
          window.localStorage.setItem('deviceModal1', json[0].devices.deviceModal)
          window.localStorage.setItem('IMEI1', json[0].devices.IMEI)
          window.localStorage.setItem('addedToChain1', json[0].addedToChain === true ? 'Yes' : 'No')
          window.localStorage.setItem('device2', json[1].devices)
          window.localStorage.setItem('deviceType2', json[1].devices.deviceType)
          window.localStorage.setItem('deviceModal2', json[1].devices.deviceModal)
          window.localStorage.setItem('IMEI2', json[1].devices.IMEI)
          window.localStorage.setItem('addedToChain2', json[1].addedToChain === true ? 'Yes' : 'No')
          window.localStorage.setItem('device3', json[2].devices)
          window.localStorage.setItem('deviceType3', json[2].devices.deviceType)
          window.localStorage.setItem('deviceModal3', json[2].devices.deviceModal)
          window.localStorage.setItem('IMEI3', json[2].devices.IMEI)
          window.localStorage.setItem('addedToChain3', json[2].addedToChain === true ? 'Yes' : 'No')
          window.localStorage.setItem('device4', json[3].devices)
          window.localStorage.setItem('deviceType4', json[3].devices.deviceType)
          window.localStorage.setItem('deviceModal4', json[3].devices.deviceModal)
          window.localStorage.setItem('IMEI4', json[3].devices.IMEI)
          window.localStorage.setItem('addedToChain4', json[3].addedToChain === true ? 'Yes' : 'No')
        })
    const device1 = window.localStorage.getItem('device1')
    const device2 = window.localStorage.getItem('device2')
    const device3 = window.localStorage.getItem('device3')
    const device4 = window.localStorage.getItem('device4')
    return (
      <div className='form-page__wrapper'>
        {!device1
        ? <NoDevices /> : null
        }
        {device1
          ? <div className='form-page__form-wrapper'>
            <div className='form-page__form-header'>
              <h2 className='form-page__form-heading'>Device 1</h2>
            </div>
            <DeviceDetailsForm1 data={formState} dispatch={dispatch} history={this.props.history} btnText={'Update'} error={error} currentlySending={currentlySending} />
          </div> : null
        }{device2
        ? <div className='form-page__wrapper'>
          <div className='form-page__form-wrapper'>
            <div className='form-page__form-header'>
              <h2 className='form-page__form-heading'>Device 2</h2>
            </div>
            <DeviceDetailsForm2 data={formState} dispatch={dispatch} onLoaded={this._getDeviceDetails} history={this.props.history} btnText={'Update'} error={error} currentlySending={currentlySending} />
          </div>
        </div> : null
      } {device3
        ? <div className='form-page__wrapper'>
          <div className='form-page__form-wrapper'>
            <div className='form-page__form-header'>
              <h2 className='form-page__form-heading'>Device 3</h2>
            </div>
            <DeviceDetailsForm3 data={formState} dispatch={dispatch} onLoaded={this._getDeviceDetails} history={this.props.history} btnText={'Update'} error={error} currentlySending={currentlySending} />
          </div>
        </div> : null
      }
        {device4
          ? <div className='form-page__wrapper'>
            <div className='form-page__form-wrapper'>
              <div className='form-page__form-header'>
                <h2 className='form-page__form-heading'>Device 4</h2>
              </div>
              <DeviceDetailsForm4 data={formState} dispatch={dispatch} onLoaded={this._getDeviceDetails} history={this.props.history} btnText={'Update'} error={error} currentlySending={currentlySending} />
            </div>
          </div> : null
        }
      </div>
    )
  }
}

DeviceDetails.propTypes = {
  data: React.PropTypes.object,
  history: React.PropTypes.object,
  dispatch: React.PropTypes.func
}

// Which props do we want to inject, given the global state?
function select (state) {
  return {
    data: state
  }
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(DeviceDetails)
