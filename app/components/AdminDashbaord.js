import React from 'react'
import DeviceTable from "./common/DeviceTable";

function AdminDashboard () {
  return (
    <article>
      <section className='text-section'>
        <h1>Admin Dashboard</h1>
          <h2>What next?</h2>
          <p>Okay so now you have user is with you and their device is present, please check that the detail below match,
          after this we can move on to the next step of adding them to the blockchain!</p>
        <DeviceTable />
      </section>
    </article>
  )
}

export default AdminDashboard
