import React, {Component} from 'react'
import {connect} from 'react-redux'
import AddDeviceForm from './common/AddDeviceForm'
import { browserHistory } from 'react-router'
const fetch = require('node-fetch')

class AddDevice extends Component {
  constructor (props) {
    super(props)

    this._addDevice = this._addDevice.bind(this)

    this.state = {
      deviceErrors: ''
    }
  }

  _addDevice (deviceType, IMEI, deviceModal) {
    fetch('http://localhost:3007/v1/device/create', {
      method: 'POST',
      body: JSON.stringify({
        deviceType: deviceType,
        deviceModal: deviceModal,
        IMEI: IMEI
      }),
      headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Accept': 'application/json'
      }
    })
        .then(res => {
          if (res.status === 200) {
            console.log(res.status)
            // forwardTo('/deviceAdded')
          } else {
            console.log('Error adding device')
          }
        }).then(
        fetch('http://localhost:3007/v1/device/' + window.localStorage.getItem('username'), {
          method: 'POST',
          body: JSON.stringify({
            devices: {
              deviceType: deviceType,
              deviceModal: deviceModal,
              IMEI: IMEI
            }
          }),
          headers: {
            'Content-Type': 'application/json; charset=utf-8',
            'Accept': 'application/json'
          }}).then(res => res.json())
            .then(json => {
          if (json.error) {
            const errorMessage = json.error[0].msg;
            this.setState({deviceErrors: errorMessage})
          } else {
            forwardTo('/deviceAdded')
          }
        })
    )
  }
  render () {
    const { dispatch } = this.props
    const { formState, currentlySending, error } = this.props.data

    return (
      <div className='form-page__wrapper'>
        <div className='form-page__form-wrapper'>
          <div className='form-page__form-header'>
            <h2 className='form-page__form-heading'>Add New Device</h2>
          </div>
          <AddDeviceForm data={formState} dispatch={dispatch} history={this.props.history} onSubmit={this._addDevice}
            btnText={'Add Device'} error={this.state.deviceErrors} currentlySending={currentlySending} />
        </div>
      </div>
    )
  }
}

AddDevice.propTypes = {
  data: React.PropTypes.object,
  history: React.PropTypes.object,
  dispatch: React.PropTypes.func
}

// Which props do we want to inject, given the global state?
function select (state) {
  return {
    data: state
  }
}
function forwardTo (location) {
  browserHistory.push(location)
}

// Wrap the component to inject dispatch and state into it
export default connect(select)(AddDevice)
