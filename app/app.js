const express = require('express')
const app = express()
const port = 3007
app.use(express.json())

const adminController = require('./controllers/AdminController');
const deviceController = require('./controllers/DeviceController');
const userController = require('./controllers/UserController');
const transactionReceipt = require('./controllers/TransactionReceiptController');

adminController.init(app);
deviceController.init(app);
userController.init(app);
transactionReceipt.init(app);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}!`)
})

require('./database/UserDatabase')
module.exports = app;

