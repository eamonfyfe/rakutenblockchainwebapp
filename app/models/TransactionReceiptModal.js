const mongoose = require('mongoose')

const Devices = require('../models/DeviceModal').schema

const TransactionReceiptSchema = new mongoose.Schema({
    username: {
        type: String,
        default: ''
    },
    devices: {
        type: Devices
    },
    transactionHash: {
        type: String,
        default: ''
    },
    blockHash: {
        type: String,
        default: ''
    },
    blockNumber: {
        type: Number,
        default: ''
    },
    destination: {
        type: String,
        default: ''
    },
})

module.exports = mongoose.model('TransactionReceipt', TransactionReceiptSchema)
