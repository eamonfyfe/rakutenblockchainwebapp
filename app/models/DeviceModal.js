const mongoose = require('mongoose')

const DeviceSchema = new mongoose.Schema({
  deviceType: {
    type: String,
    default: ''
  },
  deviceModal: {
    type: String,
    default: ''
  },
  IMEI: {
    type: String,
    default: '',
    unique: true
  }
})

module.exports = mongoose.model('Device', DeviceSchema)
