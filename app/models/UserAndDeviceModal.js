const mongoose = require('mongoose')

const Devices = require('../models/DeviceModal').schema

const UserAndDeviceSchema = new mongoose.Schema({
  username: {
    type: String,
    default: ''
  },
  devices: {
    type: Devices
  },
    addedToChain: {
      type: Boolean,
      default: false
    }
})

module.exports = mongoose.model('UserAndDevice', UserAndDeviceSchema)
